Drupal Guestbook Views (DGBV) provides Views module integration for the
Drupal Guestbook (DGB) module.

Requirements
--------------------------------------------------------------------------------
- This module is written for Drupal 6.0+.
- The Views module.
- The Drupal Guestbook (DGB) module.
  IMPORTANT: DGB 1.4+ or dev version required!

Recommended modules
--------------------------------------------------------------------------------
- The Date module.
  Read more in the documentation below.
- The jQuery UI module.

Installation
--------------------------------------------------------------------------------
1. Copy the DGBV module folder to your module directory and then enable on the
   admin modules page.
2. Copy the recommended modules to your module directory and then enable
   on the admin modules page.

Administration
--------------------------------------------------------------------------------
No administration and permission settings available.

Documentation
--------------------------------------------------------------------------------
- Views compatibility

  DGBV works with Views version 2 and 3.

- Create a guestbook view

  To add a new guestbook view use the view type "DGB entry".

- Relationships

  View type "DGB entry":
  If you configure this view without notable features it is not necessary to
  use relationships.

  View type "User":
  Use the relationship "DGB: Author". To configure fields or filters
  use "Do not use a relationship".

- Filters

  The Date module:
  To filter entries by their dates you can use the Date module. This offers
  the Date picker functionality for an exposed date filter.

- Arguments

  If you are use a filter for user IDs do not use the % wildcard in the
  view display URL. This make sure to can display all values if the URL does
  not contain an ID.

  Site guestbook:
  To filter the site guestbook use the ID 0 in the URL.
